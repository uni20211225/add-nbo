#include <stdint.h>
#include <netinet/in.h>
#include <stdio.h>

uint32_t magic(char * file_name)
{
    uint32_t num;

    //printf("%s\n", file_name);

    FILE* file = fopen(file_name, "rb");
    if (file==NULL)
    {
        printf("no file.....\n");
        return 0;
    }
    fread(&num, sizeof(uint32_t), 1, file);
    num = ntohl(num);

    fclose(file);

    return num;
}

int main(int argc, char ** argv)
{
    uint32_t num1 = magic(argv[1]);
    uint32_t num2 = magic(argv[2]);
    uint32_t result = num1+num2;
    printf("%d(%x) + %d(%x) = %d(%x)\n", num1, num1, num2, num2, result, result);
    return 0;
}
